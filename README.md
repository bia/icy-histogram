# Icy Histogram

This Icy plugin allows to compute and display the histogram of a sequence, with a more accurate control on the histogram parameters. It also enables the computation of histograms on ROIs.