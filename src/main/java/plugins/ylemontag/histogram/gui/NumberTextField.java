package plugins.ylemontag.histogram.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTextField;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component to display and modify a numeric (double) value
 */
public class NumberTextField extends JTextField
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Listener interface
	 */
	public static interface ValueListener
	{
		/**
		 * Method triggered when the numeric value in the component changes
		 */
		public void valueChanged(double newValue);
	}
	
	private double _value;
	private List<ValueListener> _listeners;

	/**
	 * Constructor
	 */
	public NumberTextField()
	{
		super();
		_value = 0;
		_listeners = new LinkedList<ValueListener>();
		addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onComponentValueChanged(getText());
			}
		});
		addFocusListener(new FocusAdapter()
		{
			@Override
			public void focusLost(FocusEvent e)
			{
				onComponentValueChanged(getText());
			}
		});
		updateComponentText();
	}
	
	/**
	 * Add a new listener
	 */
	public void addValueListener(ValueListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Remove a listener
	 */
	public void removeValueListener(ValueListener l)
	{
		_listeners.remove(l);
	}
	
	/**
	 * Retrieve the value as a double
	 */
	public double getNumericValue()
	{
		return _value;
	}
	
	/**
	 * Set the value
	 */
	public void setNumericValue(double value)
	{
		if(_value==value) {
			return;
		}
		_value = value;
		updateComponentText();
		fireValueListeners();
	}
	
	/**
	 * Update the component text to reflect the underlying numeric value
	 */
	private void updateComponentText()
	{
		setForeground(Color.BLACK);
		setText(Double.toString(_value));
	}
	
	/**
	 * Action performed when the component lost the focus or the user hit the <Enter> key
	 */
	private void onComponentValueChanged(String newText)
	{
		try {
			double oldValue = _value;
			_value = newText.isEmpty() ? 0.0 : Double.parseDouble(newText);
			updateComponentText();
			if(_value!=oldValue) {
				fireValueListeners();
			}
		}
		catch(NumberFormatException err) {
			setForeground(Color.RED);
		}
	}
	
	/**
	 * Fire the value listeners
	 */
	private void fireValueListeners()
	{
		for(ValueListener l : _listeners) {
			l.valueChanged(_value);
		}
	}
}
