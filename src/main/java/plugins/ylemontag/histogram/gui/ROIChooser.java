package plugins.ylemontag.histogram.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceAdapter;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.sequence.SequenceEvent.SequenceEventType;
import icy.sequence.SequenceListener;

/**
 * @author Yoann Le Montagner
 *         Select one ROI among the one associated to a given sequence
 */
@SuppressWarnings("rawtypes")
public class ROIChooser extends JComboBox
{
    private static final long serialVersionUID = 1L;

    /**
     * Listen to modification of the selected item
     */
    public static interface ROIChooserListener
    {
        /**
         * Called when the selected ROI changed
         */
        public void roiChanged(ROI roi);
    }

    private DefaultComboBoxModel<ROIWrapper> _model;
    private Map<ROI, ROIWrapper> _index;
    private ROIWrapper _specialItem;
    private Sequence _reference;
    private SequenceListener _listener;
    private List<ROIChooserListener> _outListeners;
    private boolean _shuntOutListeners;

    /**
     * Constructor
     */
    @SuppressWarnings("unchecked")
    public ROIChooser()
    {
        super();
        _shuntOutListeners = false;
        _outListeners = new LinkedList<ROIChooser.ROIChooserListener>();
        _index = Collections.synchronizedMap(new HashMap<ROI, ROIChooser.ROIWrapper>());
        _model = new DefaultComboBoxModel<ROIWrapper>();
        setModel(_model);
        this.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                fireOutListeners();
            }
        });
        rebuildModel();
    }

    /**
     * Add a new listener
     */
    public void addListener(ROIChooserListener l)
    {
        _outListeners.add(l);
    }

    /**
     * Remove a listener
     */
    public void removeListeneer(ROIChooserListener l)
    {
        _outListeners.remove(l);
    }

    /**
     * Return the selected ROI if any
     */
    public ROI getSelectedROI()
    {
        Object item = getSelectedItem();
        if (item != null)
        {
            return ((ROIWrapper) item).getROI();
        }
        else
        {
            return null;
        }
    }

    /**
     * Reference sequence
     */
    public Sequence getReference()
    {
        return _reference;
    }

    /**
     * Change the reference sequence
     */
    public void setReference(Sequence reference)
    {
        if (_reference == reference)
        {
            return;
        }
        if (_reference != null)
        {
            _reference.removeListener(_listener);
        }
        _reference = reference;
        if (_reference == null)
        {
            _listener = null;
        }
        else
        {
            _listener = new SequenceAdapter()
            {
                @Override
                public void sequenceChanged(SequenceEvent sequenceEvent)
                {
                    if (sequenceEvent.getSourceType() == SequenceEventSourceType.SEQUENCE_ROI)
                    {
                        ROI roi = (ROI) sequenceEvent.getSource();
                        if (sequenceEvent.getType() == SequenceEventType.ADDED)
                        {
                            registerROI(roi);
                        }
                        else if (sequenceEvent.getType() == SequenceEventType.REMOVED)
                        {
                            unregisterROI(roi);
                        }
                        else if (sequenceEvent.getType() == SequenceEventType.CHANGED)
                        {
                            checkNameModification(roi);
                        }
                    }
                }
            };
            _reference.addListener(_listener);
        }
        rebuildModel();
    }

    /**
     * Feed the model with the list of available ROIs
     */
    private void rebuildModel()
    {
        _shuntOutListeners = true;
        _model.removeAllElements();
        _index.clear();
        _specialItem = new ROIWrapper(null);
        _model.addElement(_specialItem);
        if (_reference != null)
        {
            for (ROI roi : _reference.getROIs())
            {
                registerROI(roi);
            }
        }
        _shuntOutListeners = false;
        fireOutListeners();
    }

    /**
     * Add the given ROI to the list of items, if not already present
     */
    private void registerROI(ROI roi)
    {
        if (_index.get(roi) != null)
        {
            return;
        }
        ROIWrapper wrapper = new ROIWrapper(roi);
        _model.addElement(wrapper);
        _index.put(roi, wrapper);
    }

    /**
     * Remove the given ROI from the list of items, if it exists
     */
    private void unregisterROI(ROI roi)
    {
        ROIWrapper wrapper = _index.get(roi);
        if (wrapper == null)
        {
            return;
        }
        _model.removeElement(wrapper);
        _index.remove(roi);
    }

    /**
     * Force a repaint if a ROI name has changed
     */
    private void checkNameModification(ROI roi)
    {
        ROIWrapper wrapper = _index.get(roi);
        if (wrapper != null && wrapper.checkNameModification())
        {
            repaint();
        }
    }

    /**
     * Fire the listeners
     */
    private void fireOutListeners()
    {
        if (_shuntOutListeners)
        {
            return;
        }
        ROI roi = getSelectedROI();
        for (ROIChooserListener l : _outListeners)
        {
            l.roiChanged(roi);
        }
    }

    /**
     * Wrap a ROI
     */
    private static class ROIWrapper
    {
        private ROI _roi;
        private String _name;

        public ROIWrapper(ROI roi)
        {
            _roi = roi;
            if (_roi != null)
            {
                _name = _roi.getName();
            }
        }

        public ROI getROI()
        {
            return _roi;
        }

        public boolean checkNameModification()
        {
            if (_roi == null)
            {
                return false;
            }
            String currentName = _roi.getName();
            if (_name.equals(currentName))
            {
                return false;
            }
            else
            {
                _name = currentName;
                return true;
            }
        }

        @Override
        public String toString()
        {
            return _name == null ? "whole sequence" : _name;
        }
    }
}
