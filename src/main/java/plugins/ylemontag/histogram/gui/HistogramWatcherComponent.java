package plugins.ylemontag.histogram.gui;

import plugins.ylemontag.histogram.HistogramWatcher;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component displaying an histogram managed through an 'HistogramWatcher' object
 */
public class HistogramWatcherComponent extends AbstractHistogramComponent
{
	private static final long serialVersionUID = 1L;
	
	private HistogramWatcher.ChangeListener _listener;
	private HistogramWatcher                _watcher ;
	
	/**
	 * Constructor
	 */
	public HistogramWatcherComponent()
	{
		super();
	}
	
	/**
	 * Return the current watcher
	 */
	public HistogramWatcher getWatcher()
	{
		return _watcher;
	}
	
	/**
	 * Set the histogram watcher
	 */
	public void setWatcher(HistogramWatcher watcher)
	{
		if(_watcher!=null) {
			_watcher.removeListeneer(_listener);
		}
		_watcher = watcher;
		if(_watcher==null) {
			_listener = null;
		}
		else {
			_listener = new HistogramWatcher.ChangeListener()
			{
				@Override
				public void histogramChanged() {
					refresh();
				}
			};
			_watcher.addListener(_listener);
		}
		refresh();
	}
	
	@Override
	protected void refresh()
	{
		if(_watcher==null) {
			_histogram = null;
		}
		else {
			_histogram = _watcher.getResult();
		}
		super.refresh();
	}
}
