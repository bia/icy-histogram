package plugins.ylemontag.histogram.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.SimpleHistogramBin;
import org.jfree.data.statistics.SimpleHistogramDataset;

import plugins.ylemontag.histogram.Histogram;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Abstract component displaying an object 'Histogram'
 */
public abstract class AbstractHistogramComponent extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	protected Histogram              _histogram;
	private   SimpleHistogramDataset _dataset;
	private   JFreeChart             _chart;
	
	/**
	 * Constructor
	 */
	protected AbstractHistogramComponent()
	{
		super();
		_dataset = new SimpleHistogramDataset("(gray level, sample count)");
		_dataset.setAdjustForBinSize(false);
		_chart = ChartFactory.createHistogram("", "Gray levels", "Sample count", _dataset,
			PlotOrientation.VERTICAL, false, true, false);
		XYBarRenderer renderer = (XYBarRenderer)_chart.getXYPlot().getRenderer();
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);
		ChartPanel chartPanel = new ChartPanel(_chart);
		setLayout(new BorderLayout(0, 0));
		add(chartPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Refresh the component
	 */
	protected void refresh()
	{
		_chart.getXYPlot().getDomainAxis().setAutoRange(false);
		_chart.getXYPlot().getRangeAxis ().setAutoRange(false);
		if(_histogram==null) {
			_dataset.removeAllBins();
		}
		else {
			_dataset.removeAllBins();
			int nbBins = _histogram.getNbBins();
			for(int k=0; k<nbBins; ++k) {
				Histogram.Bin      bin  = _histogram.getBin(k);
				SimpleHistogramBin jBin = new SimpleHistogramBin(bin.getLowerBound(), bin.getUpperBound(), true, false);
				jBin.setItemCount(bin.getCount());
				_dataset.addBin(jBin);
			}
		}
		_chart.getXYPlot().getDomainAxis().setAutoRange(true);
		_chart.getXYPlot().getRangeAxis ().setAutoRange(true);
	}
}
