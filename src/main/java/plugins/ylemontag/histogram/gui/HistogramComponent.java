package plugins.ylemontag.histogram.gui;

import plugins.ylemontag.histogram.Histogram;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component displaying an object 'Histogram'
 */
public class HistogramComponent extends AbstractHistogramComponent
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public HistogramComponent()
	{
		super();
	}
	
	/**
	 * Return the currently displayed histogram
	 */
	public Histogram getHistogram()
	{
		return _histogram;
	}
	
	/**
	 * Set the histogram
	 */
	public void setHistogram(Histogram histogram)
	{
		_histogram = histogram;
		refresh();
	}
}
