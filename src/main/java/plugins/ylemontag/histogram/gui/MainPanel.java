package plugins.ylemontag.histogram.gui;

import icy.gui.component.sequence.SequenceChooser;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;

public class MainPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	private HistogramWatcherComponent _histogramComponent;
	private SequenceChooser _sequenceChooser;
	private ROIChooser _roiChooser;
	private JSpinner _nbBinsComponent;
	private JCheckBox _inclusiveComponent;
	private NumberTextField _lowerBinComponent;
	private NumberTextField _upperBinComponent;
	
	public SequenceChooser getSequenceChooser()
	{
		return _sequenceChooser;
	}
	
	public ROIChooser getROIChooser()
	{
		return _roiChooser;
	}
	
	public JCheckBox getInclusiveComponent()
	{
		return _inclusiveComponent;
	}
	
	public JSpinner getNbBinsComponent()
	{
		return _nbBinsComponent;
	}
	
	public NumberTextField getLowerBinComponent()
	{
		return _lowerBinComponent;
	}
	
	public NumberTextField getUpperBinComponent()
	{
		return _upperBinComponent;
	}
	
	public HistogramWatcherComponent getHistogramComponent()
	{
		return _histogramComponent;
	}

	/**
	 * Create the panel
	 */
	public MainPanel()
	{
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(5, 5));
		
		_histogramComponent = new HistogramWatcherComponent();
		add(_histogramComponent, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new BorderLayout(5, 5));
		
		JLabel lblNewLabel = new JLabel("Sequence");
		lblNewLabel.setToolTipText("Input sequence");
		panel_2.add(lblNewLabel, BorderLayout.WEST);
		
		_sequenceChooser = new SequenceChooser();
		_sequenceChooser.setToolTipText("Input sequence");
		panel_2.add(_sequenceChooser, BorderLayout.CENTER);
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel_1.add(horizontalStrut);
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new BorderLayout(5, 5));
		
		JLabel lblNewLabel_1 = new JLabel("ROI");
		lblNewLabel_1.setToolTipText("Compute the histogram either from the whole sequence or only from the pixels within a ROI");
		panel_3.add(lblNewLabel_1, BorderLayout.WEST);
		
		_roiChooser = new ROIChooser();
		_roiChooser.setToolTipText("Compute the histogram either from the whole sequence or only from the pixels within a ROI");
		panel_3.add(_roiChooser, BorderLayout.CENTER);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel_1.add(horizontalStrut_1);
		
		_inclusiveComponent = new JCheckBox("Include borders");
		_inclusiveComponent.setToolTipText("Whether the pixels that are only partially included in the ROI should be considered in the histogram");
		panel_1.add(_inclusiveComponent);
		
		Component verticalStrut = Box.createVerticalStrut(5);
		panel.add(verticalStrut);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5);
		panel_5.setLayout(new BorderLayout(5, 5));
		
		JLabel lblNewLabel_2 = new JLabel("Number of bins");
		lblNewLabel_2.setToolTipText("Number of bins in the histogram");
		panel_5.add(lblNewLabel_2, BorderLayout.WEST);
		
		_nbBinsComponent = new JSpinner();
		_nbBinsComponent.setToolTipText("Number of bins in the histogram");
		panel_5.add(_nbBinsComponent, BorderLayout.CENTER);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		panel_4.add(horizontalStrut_2);
		
		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		panel_6.setLayout(new BorderLayout(5, 5));
		
		JLabel lblNewLabel_3 = new JLabel("Lower bin");
		lblNewLabel_3.setToolTipText("Central value of the lower bin");
		panel_6.add(lblNewLabel_3, BorderLayout.WEST);
		
		_lowerBinComponent = new NumberTextField();
		_lowerBinComponent.setToolTipText("Central value of the lower bin");
		panel_6.add(_lowerBinComponent, BorderLayout.CENTER);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(5);
		panel_4.add(horizontalStrut_3);
		
		JPanel panel_7 = new JPanel();
		panel_4.add(panel_7);
		panel_7.setLayout(new BorderLayout(5, 5));
		
		JLabel lblNewLabel_4 = new JLabel("Upper bin");
		lblNewLabel_4.setToolTipText("Central value of the upper bin");
		panel_7.add(lblNewLabel_4, BorderLayout.WEST);
		
		_upperBinComponent = new NumberTextField();
		_upperBinComponent.setToolTipText("Central value of the upper bin");
		panel_7.add(_upperBinComponent, BorderLayout.CENTER);
		
	}

}
