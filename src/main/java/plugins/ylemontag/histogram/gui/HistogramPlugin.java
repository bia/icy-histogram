package plugins.ylemontag.histogram.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import icy.gui.component.sequence.SequenceChooser;
import icy.gui.component.sequence.SequenceChooser.SequenceChooserListener;
import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.IcyFrameAdapter;
import icy.gui.frame.IcyFrameEvent;
import icy.gui.main.ActiveSequenceListener;
import icy.main.Icy;
import icy.plugin.abstract_.PluginActionable;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceEvent;
import plugins.ylemontag.histogram.BadHistogramParameters;
import plugins.ylemontag.histogram.HistogramWatcher;
import plugins.ylemontag.histogram.gui.NumberTextField.ValueListener;
import plugins.ylemontag.histogram.gui.ROIChooser.ROIChooserListener;

/**
 * @author Yoann Le Montagner
 *         Plugin to compute and display an histogram from the values of the samples
 *         located within a given ROI
 */
public class HistogramPlugin extends PluginActionable
{
    private SequenceChooser _sequenceChooser;
    private ROIChooser _roiChooser;
    private JCheckBox _inclusiveComponent;
    private JSpinner _nbBinsComponent;
    private NumberTextField _lowerBinComponent;
    private NumberTextField _upperBinComponent;
    private HistogramWatcherComponent _histogramComponent;

    @Override
    public void run()
    {
        // GUI
        IcyFrame frame = new IcyFrame("Histogram", true, true, true, true);
        frame.setSize(600, 450);
        frame.setSizeExternal(600, 450);
        MainPanel mainPanel = new MainPanel();
        frame.add(mainPanel);
        addIcyFrame(frame);
        frame.center();
        frame.setVisible(true);
        frame.requestFocus();
        _sequenceChooser = mainPanel.getSequenceChooser();
        _roiChooser = mainPanel.getROIChooser();
        _inclusiveComponent = mainPanel.getInclusiveComponent();
        _nbBinsComponent = mainPanel.getNbBinsComponent();
        _lowerBinComponent = mainPanel.getLowerBinComponent();
        _upperBinComponent = mainPanel.getUpperBinComponent();
        _histogramComponent = mainPanel.getHistogramComponent();
        _sequenceChooser.addListener(new SequenceChooserListener()
        {
            @Override
            public void sequenceChanged(Sequence sequence)
            {
                _roiChooser.setReference(sequence);
            }
        });
        _roiChooser.addListener(new ROIChooserListener()
        {
            @Override
            public void roiChanged(ROI roi)
            {
                refreshInclusiveComponentState();
                rebuildWatcher();
            }
        });
        _inclusiveComponent.setSelected(true);
        _inclusiveComponent.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                rebuildWatcher();
            }
        });
        refreshInclusiveComponentState();
        _nbBinsComponent.setModel(new SpinnerNumberModel(256, 2, 99999, 1));
        _nbBinsComponent.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                rebuildWatcher();
            }
        });
        _lowerBinComponent.setNumericValue(0);
        _lowerBinComponent.addValueListener(new ValueListener()
        {
            @Override
            public void valueChanged(double newValue)
            {
                rebuildWatcher();
            }
        });
        _upperBinComponent.setNumericValue(255);
        _upperBinComponent.addValueListener(new ValueListener()
        {
            @Override
            public void valueChanged(double newValue)
            {
                rebuildWatcher();
            }
        });
        _sequenceChooser.setSelectedSequence(Icy.getMainInterface().getActiveSequence());
        ActiveSequenceListener sequenceChangeListener = new ActiveSequenceListener()
        {

            @Override
            public void sequenceDeactivated(Sequence sequence)
            {// Nothing to do here
            }

            @Override
            public void sequenceActivated(Sequence sequence)
            {
                if (_sequenceChooser.getSelectedIndex() <= 0)
                {
                    _sequenceChooser.setSelectedSequence(sequence);
                    _sequenceChooser.setActiveSequenceSelected();
                }
            }

            @Override
            public void activeSequenceChanged(SequenceEvent event)
            {// Nothing to do here
            }
        };
        Icy.getMainInterface().addActiveSequenceListener(sequenceChangeListener);

        frame.addFrameListener(new IcyFrameAdapter()
        {
            @Override
            public void icyFrameClosed(IcyFrameEvent e)
            {
                Icy.getMainInterface().removeActiveSequenceListener(sequenceChangeListener);
            }
        });
    }

    /**
     * Rebuild the histogram
     */
    private void rebuildWatcher()
    {
        Sequence seq = _sequenceChooser.getSelectedSequence();
        ROI roi = _roiChooser.getSelectedROI();
        boolean inclusive = _inclusiveComponent.isSelected();
        double minBin = _lowerBinComponent.getNumericValue();
        double maxBin = _upperBinComponent.getNumericValue();
        int nbBins = (Integer) _nbBinsComponent.getValue();
        HistogramWatcher watcher = null;
        if (seq != null)
        {
            try
            {
                watcher = new HistogramWatcher(seq, roi, inclusive, nbBins, minBin, maxBin);
            }
            catch (BadHistogramParameters err)
            {
                String message = err.getMessage();
                MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
            }
        }
        _histogramComponent.setWatcher(watcher);
    }

    /**
     * Activate/inactivate the 'inclusive' checkbox depending on whether a ROI is selected
     */
    private void refreshInclusiveComponentState()
    {
        boolean roiSelected = (_roiChooser.getSelectedROI() != null);
        _inclusiveComponent.setEnabled(roiSelected);
    }
}
