package plugins.ylemontag.histogram;

import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceAdapter;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.system.thread.ThreadUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Compute an histogram on a sequence, and listen to the sequence changed events
 * to update the histogram
 */
public class HistogramWatcher
{
	/**
	 * Listen to the value change
	 */
	public static interface ChangeListener
	{
		/**
		 * Method called when a modification occurs in the histogram
		 */
		public void histogramChanged();
	}
	
	private Sequence _seq      ;
	private ROI      _roi      ;
	private boolean  _inclusive;
	private int      _nbBins   ;
	private double   _minBin   ;
	private double   _maxBin   ;
	
	private List<ChangeListener> _listeners;
	private Histogram            _result;
	private ReentrantLock        _mutex;
	private Controller           _controller;
	
	/**
	 * Constructor
	 * @param seq Targeted sequence
	 * @param nbBins Number of bins
	 * @param minBin Central value of the lower bin
	 * @param maxBin Central value of the upper bin
	 */
	public HistogramWatcher(Sequence seq, int nbBins, double minBin, double maxBin)
		throws BadHistogramParameters
	{
		this(seq, null, true, nbBins, minBin, maxBin);
	}
	
	/**
	 * Constructor
	 * @param seq Targeted sequence
	 * @param roi Only the samples within the ROI are taken into account in the
	 *        histogram (set to null to select the whole sequence)
	 * @param inclusive Whether the samples that are only partially included in
	 *        the ROI should be taken into account
	 * @param nbBins Number of bins
	 * @param minBin Central value of the lower bin
	 * @param maxBin Central value of the upper bin
	 */
	public HistogramWatcher(Sequence seq, ROI roi, boolean inclusive, int nbBins, double minBin, double maxBin)
		throws BadHistogramParameters
	{
		Histogram.ensureValidParameters(nbBins, minBin, maxBin);
		_seq       = seq      ;
		_roi       = roi      ;
		_inclusive = inclusive;
		_nbBins    = nbBins   ;
		_minBin    = minBin   ;
		_maxBin    = maxBin   ;
		_listeners = new LinkedList<HistogramWatcher.ChangeListener>();
		_mutex     = new ReentrantLock();
		_seq.addListener(new SequenceAdapter()
		{
			@Override
			public void sequenceChanged(SequenceEvent sequenceEvent) {
				if(sequenceEvent.getSourceType()==SequenceEventSourceType.SEQUENCE_DATA) {
					refreshHistogram();
				}
				else if(sequenceEvent.getSourceType()==SequenceEventSourceType.SEQUENCE_ROI) {
					if(sequenceEvent.getSource()==_roi) {
						refreshHistogram();
					}
				}
			}
		});
		refreshHistogram();
	}
	
	/**
	 * Add a new listener
	 */
	public void addListener(ChangeListener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Remove a listener
	 */
	public void removeListeneer(ChangeListener l)
	{
		_listeners.remove(l);
	}
	
	/**
	 * Return the underlying sequence
	 */
	public Sequence getSequence()
	{
		return _seq;
	}
	
	/**
	 * Return the underlying ROI
	 */
	public ROI getROI()
	{
		return _roi;
	}
	
	/**
	 * Whether the histogram should take into account the samples that are partially
	 * included in the ROI
	 */
	public boolean getInclusive()
	{
		return _inclusive;
	}
	
	/**
	 * Return the result if ready, otherwise return null
	 */
	public Histogram getResult()
	{
		if(_mutex.tryLock()) {
			try {
				return _result;
			}
			finally {
				_mutex.unlock();
			}
		}
		else {
			return null;
		}
	}
	
	/**
	 * Refresh the histogram (asynchronous function)
	 */
	private void refreshHistogram()
	{
		if(_controller!=null) {
			_controller.cancelComputation();
		}
		_controller = new Controller();
		ThreadUtil.bgRun(new Runnable()
		{
			@Override
			public void run() {
				boolean fireListeners = true;
				_mutex.lock();
				try
				{
					// Clean the previous result 
					_result = null;
					
					// Compute the new histogram
					if(_roi==null) {
						_result = Histogram.compute(_seq, _nbBins, _minBin, _maxBin, _controller);
					}
					else {
						_result = Histogram.compute(_seq, _roi, _inclusive, _nbBins, _minBin, _maxBin, _controller);
					}
				}
				catch(BadHistogramParameters err) {
					throw new RuntimeException("Unreachable code point");
				}
				catch(Controller.CanceledByUser err) {
					fireListeners = false;
				}
				finally {
					_mutex.unlock();
					if(fireListeners) {
						ThreadUtil.invokeLater(new Runnable()
						{
							@Override
							public void run() {
								fireListeners();
							}
						});
					}
				}
			}
		});
	}
	
	/**
	 * Fire the listeners
	 */
	private void fireListeners()
	{
		for(ChangeListener l : _listeners) {
			l.histogramChanged();
		}
	}
}
