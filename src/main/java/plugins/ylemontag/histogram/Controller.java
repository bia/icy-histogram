package plugins.ylemontag.histogram;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Object used to control and interrupt histogram computation
 */
public class Controller
{	
	/**
	 * Exception thrown when a SSIM computation have been interrupted
	 */
	public static class CanceledByUser extends Exception
	{
		private static final long serialVersionUID = 1L;
	}
	
	private boolean _cancelFlag;
	
	/**
	 * Constructor
	 */
	public Controller()
	{
		_cancelFlag = false;
	}
	
	/**
	 * Request that current histogram computation stops as soon as possible
	 */
	public void cancelComputation()
	{
		_cancelFlag = true;
	}
	
	/**
	 * Check-point function called by the thread
	 */
	void checkPoint() throws CanceledByUser
	{
		if(_cancelFlag) {
			throw new CanceledByUser();
		}
	}
}
