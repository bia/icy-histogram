package plugins.ylemontag.histogram.blocks;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.util.VarException;
import plugins.ylemontag.histogram.BadHistogramParameters;
import plugins.ylemontag.histogram.Histogram;
import plugins.ylemontag.histogram.gui.HistogramPlugin;

/**
 * @author Yoann Le Montagner
 *         Compute the histogram of a sequence
 */
public class HistogramBlock extends Plugin implements Block, PluginBundled
{
    private VarSequence _sequence = new VarSequence("Sequence", null);
    private VarInteger _nbBins = new VarInteger("Number of bins", 256);
    private VarDouble _lowerBin = new VarDouble("Lower bin", 0);
    private VarDouble _upperBin = new VarDouble("Upper bin", 255);
    private VarHistogram _histogram = new VarHistogram("Histogram", null);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add(_sequence.getName(), _sequence);
        inputMap.add(_nbBins.getName(), _nbBins);
        inputMap.add(_lowerBin.getName(), _lowerBin);
        inputMap.add(_upperBin.getName(), _upperBin);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add(_histogram.getName(), _histogram);
    }

    @Override
    public String getMainPluginClassName()
    {
        return HistogramPlugin.class.getName();
    }

    @Override
    public String getName()
    {
        return "Histogram";
    }

    @Override
    public void run()
    {
        // Retrieve the parameters
        Sequence sequence = _sequence.getValue();
        int nbBins = _nbBins.getValue();
        double lowerBin = _lowerBin.getValue();
        double upperBin = _upperBin.getValue();
        if (sequence == null)
        {
            throw new VarException(_sequence, "No sequence selected");
        }

        // Compute the histogram and set the result
        try
        {
            Histogram histogram = Histogram.compute(sequence, nbBins, lowerBin, upperBin);
            _histogram.setValue(histogram);
        }
        catch (BadHistogramParameters err)
        {
            throw new VarException(_histogram, err.getMessage());
        }
    }
}
