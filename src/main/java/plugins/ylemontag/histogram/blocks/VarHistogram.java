package plugins.ylemontag.histogram.blocks;

import javax.swing.JComponent;

import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.ylemontag.histogram.Histogram;
import plugins.ylemontag.histogram.gui.HistogramComponent;

/**
 * @author Yoann Le Montagner
 *         Implements the Vars framework for 'Histogram' objects
 */
public class VarHistogram extends Var<Histogram>
{
    /**
     * Editor
     */
    private static class Editor extends SwingVarEditor<Histogram>
    {
        private HistogramComponent _component;

        public Editor(Var<Histogram> variable)
        {
            super(variable);
        }

        @Override
        protected JComponent createEditorComponent()
        {
            _component = new HistogramComponent();
            return _component;
        }

        @Override
        protected void activateListeners()
        {
            // Nothing to do as the component cannot modify the underlying Histogram object
        }

        @Override
        protected void deactivateListeners()
        {
            // Nothing to do as the component cannot modify the underlying Histogram object
        }

        @Override
        protected void updateInterfaceValue()
        {
            _component.setHistogram(variable.getValue());
        }
    }

    public VarHistogram(String name, Histogram defaultValue)
    {
        super(name, Histogram.class, defaultValue, null);
    }

    @Override
    public VarEditor<Histogram> createVarViewer()
    {
        return new Editor(this);
    }
}
