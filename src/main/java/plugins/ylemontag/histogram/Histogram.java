package plugins.ylemontag.histogram;

import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import plugins.ylemontag.histogram.Controller.CanceledByUser;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Histogram class (holds a list of bins), and histogram generation functions
 */
public class Histogram
{
	/**
	 * Computes an histogram on a whole sequence
	 * @param seq Targeted sequence
	 * @param nbBins Number of bins
	 * @param minBin Central value of the lower bin
	 * @param maxBin Central value of the upper bin
	 */
	public static Histogram compute(Sequence seq, int nbBins, double minBin, double maxBin)
		throws BadHistogramParameters
	{
		try {
			return new Histogram(seq, null, true, nbBins, minBin, maxBin, new Controller());
		}
		catch (CanceledByUser e) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Computes an histogram on a sub-region of a sequence
	 * @param seq Targeted sequence
	 * @param roi Only the samples within the ROI are taken into account in the
	 *        histogram (set to null to select the whole sequence)
	 * @param inclusive Whether the samples that are only partially included in
	 *        the ROI should be taken into account
	 * @param nbBins Number of bins
	 * @param minBin Central value of the lower bin
	 * @param maxBin Central value of the upper bin
	 */
	public static Histogram compute(Sequence seq, ROI roi, boolean inclusive,
		int nbBins, double minBin, double maxBin)
		throws BadHistogramParameters
	{
		try {
			return new Histogram(seq, roi, inclusive, nbBins, minBin, maxBin, new Controller());
		}
		catch (CanceledByUser e) {
			throw new RuntimeException("Unreachable code point");
		}
	}
	
	/**
	 * Computes an histogram on a whole sequence
	 * @param seq Targeted sequence
	 * @param nbBins Number of bins
	 * @param minBin Central value of the lower bin
	 * @param maxBin Central value of the upper bin
	 * @param controller Object that can be used to interrupt the computation
	 */
	public static Histogram compute(Sequence seq, int nbBins, double minBin, double maxBin,
		Controller controller)
		throws BadHistogramParameters, Controller.CanceledByUser
	{
		return new Histogram(seq, null, true, nbBins, minBin, maxBin, controller);
	}
	
	/**
	 * Computes an histogram on a sub-region of a sequence
	 * @param seq Targeted sequence
	 * @param roi Only the samples within the ROI are taken into account in the
	 *        histogram
	 * @param inclusive Whether the samples that are only partially included in
	 *        the ROI should be taken into account
	 * @param nbBins Number of bins
	 * @param minBin Central value of the lower bin
	 * @param maxBin Central value of the upper bin
	 * @param controller Object that can be used to interrupt the computation
	 */
	public static Histogram compute(Sequence seq, ROI roi, boolean inclusive,
		int nbBins, double minBin, double maxBin, Controller controller)
		throws BadHistogramParameters, Controller.CanceledByUser
	{
		return new Histogram(seq, roi, inclusive, nbBins, minBin, maxBin, controller);
	}
	
	/**
	 * Make sure that the histogram parameters are valid
	 */
	public static void ensureValidParameters(int nbBins, double minBin, double maxBin)
		throws BadHistogramParameters
	{
		computeBinWidth(nbBins, minBin, maxBin);
	}
	
	/**
	 * Bin
	 */
	public static class Bin
	{
		private int    _count     ;
		private double _lowerBound;
		private double _upperBound;
		
		/**
		 * Constructor
		 */
		private Bin(int count, double lowerBound, double upperBound)
		{
			 _count      = count     ;
			 _lowerBound = lowerBound;
			 _upperBound = upperBound;
		}
		
		/**
		 * Number of items that belong to this bin
		 */
		public int getCount()
		{
			return _count;
		}
		
		/**
		 * Lower bound
		 */
		public double getLowerBound()
		{
			return _lowerBound;
		}
		
		/**
		 * Upper bound
		 */
		public double getUpperBound()
		{
			return _upperBound;
		}
		
		/**
		 * Central value
		 */
		public double getCentralValue()
		{
			return (_upperBound + _lowerBound) / 2.0;
		}
	}
	
	private Bin[]  _bins    ;
	private double _minBin  ;
	private double _maxBin  ;
	private double _binWidth;
	
	/**
	 * Number of bins
	 */
	public int getNbBins()
	{
		return _bins.length;
	}
	
	/**
	 * Return the idx^th bin
	 */
	public Bin getBin(int idx)
	{
		return _bins[idx];
	}
	
	/**
	 * Value corresponding to the center of the lowest bin
	 */
	public double getMinBin()
	{
		return _minBin;
	}
	
	/**
	 * Value corresponding to the center of the highest bin
	 */
	public double getMaxBin()
	{
		return _maxBin;
	}
	
	/**
	 * Return the bin width
	 */
	public double getBinWidth()
	{
		return _binWidth;
	}
	
	/**
	 * Constructor (computes the histogram)
	 */
	private Histogram(Sequence seq, ROI roi, boolean inclusive, int nbBins, double minBin, double maxBin,
		Controller controller) throws BadHistogramParameters, Controller.CanceledByUser
	{
		// Initialization
		_minBin     = minBin;
		_maxBin     = maxBin;
		_binWidth   = computeBinWidth(nbBins, minBin, maxBin);
		int[] count = new int[nbBins];
		controller.checkPoint();
		
		// Visit all the selected samples
		SequenceDataIterator it;
		if(roi==null) {
			it = new SequenceDataIterator(seq);
		}
		else {
			it = new SequenceDataIterator(seq, roi, inclusive);
		}
		for(; !it.done(); it.next()) {
			controller.checkPoint();
			int currentIndex = computeBinIndex(it.get());
			if(currentIndex>=0 && currentIndex<nbBins) {
				++count[currentIndex];
			}
		}
		
		// Set the result
		_bins = new Bin[nbBins];
		double bound = _minBin - 0.5*_binWidth;
		for(int k=0; k<nbBins; ++k) {
			double newBound = bound + _binWidth;
			_bins[k] = new Bin(count[k], bound, newBound);
			bound = newBound;
		}
	}
	
	/**
	 * Return the bin index corresponding to a given value
	 * @warning The function may return an index <0 or >=_nbBins, which means that
	 *          the corresponding sample value lies outside the bounds of the histogram 
	 */
	private int computeBinIndex(double value)
	{
		return (int)Math.round((value - _minBin) /  _binWidth);
	}
	
	/**
	 * Refresh the value of the bin width
	 */
	private static double computeBinWidth(int nbBins, double minBin, double maxBin)
		throws BadHistogramParameters
	{
		if(maxBin<=minBin) {
			throw new BadHistogramParameters("The upper bin value must be strictly greater than the lower bin value");
		}
		else if(nbBins<=1) {
			throw new BadHistogramParameters("The number of bins must be >= 2");
		}
		return (maxBin - minBin) / (nbBins - 1);
	}
}
