package plugins.ylemontag.histogram;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Exception thrown when trying to create an histogram with invalid parameter values
 */
public class BadHistogramParameters extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public BadHistogramParameters()
	{
		super();
	}
	
	public BadHistogramParameters(String message)
	{
		super(message);
	}
	
	public BadHistogramParameters(Throwable throwable)
	{
		super(throwable);
	}
}
